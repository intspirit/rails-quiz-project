class QuizzesController < ApplicationController
  def index
  	@tests = Quiz.all
  	@title = "Tests"
    Time::DATE_FORMATS[:limit] = "%M"
  end

  def show
  	@test = Quiz.find(params[:id])
  	@title = @test.title
  end

  def new
  end

  def edit
  end
end
