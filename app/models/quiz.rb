# == Schema Information
#
# Table name: tests
#
#  id         :integer          not null, primary key
#  title      :string(45)
#  time       :time
#  status     :integer
#  created_at :datetime
#  updated_at :string(45)
#

class Quiz < ActiveRecord::Base
	set_table_name "tests"
	attr_accessible :tilte, :time, :status, :test_id
	has_many :questions, :foreign_key => "test_id",:dependent => :destroy
end
