# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  email        :string(45)
#  display_name :string(45)
#  skype        :string(45)
#  country      :string(45)
#  password     :string(45)
#  salt         :string(45)
#  created_at   :datetime
#  updated_at   :datetime
#

class User < ActiveRecord::Base
	attr_accessible :name, :email, :display_name, :skype , :country, :password, :salt
end
