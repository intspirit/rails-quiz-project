# == Schema Information
#
# Table name: questions
#
#  id            :integer          not null, primary key
#  test_id       :integer
#  text          :string(45)
#  type_question :integer          default(0), not null
#  created_at    :datetime
#  updated_at    :datetime
#

class Question < ActiveRecord::Base

	belongs_to :test, :class_name => "Quiz"

	attr_accessible :id, :test_id, :text, :type_question

	validates :text, :presence => true, :length => { :maximum => 340 }
    validates :test_id, :presence => true

	default_scope :order => 'questions.id DESC'
end
