TestProject::Application.routes.draw do

  get "quizzes/index"

  get "quizzes/new"

  get "quizzes/edit"

  root              :to => 'quizzes#index'
  match '/contact', :to => 'pages#contact'
  match '/about',   :to => 'pages#about'
  match '/help',    :to => 'pages#help'
  match 'signup',   :to => 'users#new'

  get "users/new"

end
