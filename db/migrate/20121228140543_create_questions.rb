class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :test_id
      t.string :text
      t.integer :type

      t.timestamps
    end
  end
end
