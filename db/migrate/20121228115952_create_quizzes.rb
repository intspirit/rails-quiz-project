class CreateQuizzes < ActiveRecord::Migration
  def change
    create_table :quizzes do |t|
      t.string :title
      t.integer :time
      t.integer :status

      t.timestamps
    end
  end
end
